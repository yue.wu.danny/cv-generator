module.exports = {
  name: "Siwen Liu",
  intro: "Creative and expressive individual, coming from a background as a qualified ECE teacher to take the adventure of the data analysis world.",
  contact: [
    {
      title: "Mobile",
      content: "021-027-599-08"
    },
    {
      title: "Mail",
      content: "Lindsay198888@gmail.com"
    },
    {
      title: "Home",
      content: "566L Adelaide Road, Berhampore Wellington 6023",
    },
  ],
  social: [

  ],
  experiences: [
    {
      company: "Victoria University of Wellington",
      period: "Jan 2019 - PRESENT",
      title: "Full-time Student",
      desc: "Working on an interesting survey design assignment, as well as teaching myself mathematical logic and practical analytical skills, is both challenging and rewarding. It's quite different to my previous studies and employment, but I am really enjoying it."
    },
    {
      company: "Kindercare Learning Centres Kilbirnie",
      period: "May 2017 - Oct 2018",
      title: "Full-time Teacher (under 2s)",
      desc: "Working closely with parents to provide individualised and high quality care and education to children under 2 years of age. Implementing creative experiences into the programme to encourage multi-intelligence. Building effective relationships with children and families to better meet their needs and expectations. Writing learning stories and daily updates. Gaining great experience in the Educa online portfolio system. Participating in and contributing to the self-review process and planning meetings. Further developing teaching pedagogies for under 2s. Actively seeking ways to improve my practice."
    }
  ],
  educations: [
    {
      major: "Applied Statistics, SAS, R, Linear model and multi-variance analysis.",
      degree: "Graduate Diploma in Science of  Statistics",
      college: "Victoria University of Wellington",
      period: "Jan 2019 - Dec 2019",
    },
    {
      major: "Early Childhood Education (ECE)",
      degree: "Graduate Diploma in Teaching",
      college: "New Zealand Tertiary College (Auckland)",
      period: "2015 - 2017",
    },
    {
      major: "International Comparative Education (ICE)",
      degree: "Master in Education",
      college: "Guangxi Normal University (China)",
      period: "2011 - 2014",
    }
  ],
  skills:[
    {
      name: "R",
      score: 7
    },
    {
      name: "Office 365",
      score: 8
    },
    {
      name: "SAS",
      score: 6
    },
    {
      name: "HTML",
      score: 7
    }
  ]
}
